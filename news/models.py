from django.db import models

# Create your models here.
class News(models.Model):
    """docstring for News"""
    headline = models.CharField(max_length=500)
    byline = models.CharField(max_length=1500)
    author = models.CharField(max_length=255)
    body = models.TextField()
    picture_url = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Comment(models.Model):
    comment = models.TextField()
    username = models.CharField(max_length=255)
    news = models.ForeignKey(News, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
